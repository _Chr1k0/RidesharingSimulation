'''
TODO: Header
'''

# Section: Necessary Imports
# ----------------------------------------------------------------------------------------------------------------------
import os
import sys
import Tkinter, tkFileDialog
# ----------------------------------------------------------------------------------------------------------------------

def getdirectory():
    root = Tkinter.Tk()
    root.withdraw()
    return tkFileDialog.askdirectory()  


def displayhelp():
    print("HEEELP - How to use?\n"
          "    0. Make sure the you put all files you want to parse into one directory (and just them - no other files are allowed!)\n"
          "    1. Choose that directory\n"
          "    2. let the parser do his job\n"
          "    3. Done! :)")
    

def main():
    displayhelp()
    
    dir = getdirectory()
    #dir = "Test\\"
    if not dir:   
        print("\nExiting!")
        sys.exit()
    else:        
        print("\nChosen directory: " + dir)

        filestoparse = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        print("Number of files found: " + str(len(filestoparse)) + "\n")
        #print("Files: " + str(filestoparse))

        filenum = 1
        totfilenum = len(filestoparse)
        for f in filestoparse:
            print("Parsing files (" + str(filenum) + "/" + str(totfilenum) +"), please wait")#, end="\r")         
            
            filetoread = os.path.join(dir, f) 
            f0 = open(os.path.join(dir, "split", "0-" + f), "w")
            f1 = open(os.path.join(dir, "split", "1-" + f), "w")

            for line in open(filetoread):
                if line.startswith("Parameters:") or line.startswith("0;"):
                    f0.write(line)
                if line.startswith("Parameters:") or line.startswith("1;"):
                    f1.write(line)
                       
            filenum += 1

    print("\nExiting!")   
       
        
        
        
if __name__ == "__main__":
    main()
