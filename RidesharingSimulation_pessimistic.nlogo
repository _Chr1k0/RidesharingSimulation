; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
; TODO: Header

; TODO/possible improvements:
; - sepearte output in two files
; - consider a satellite parking fee?
; - tracking the type of a person in the simulation (what a person wants to be vs. what they become)
; - (more) code comments and structure in logic section
; - input validation?!
; - proper error handling?!
; - save/load (default) configuartion/input values?!
; - make input sliders?!
; - improve the graphical visualisation....

; - can solo drivers also get ridesharedriers on the way back?!
; - rideshare drivers waiting at the stallite/central x seconds for passengers?!
;
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; global variables section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
globals [
  ;display? ;(switch) boolean indicating whether to display a graphical visualization of the simulation or not
  ;ridesharing? ;(switch) boolean indicating whether to run the simulation with (true) or without (false) ridesharing
  ;update-interval ;(input) the time interval in seconds each tick of the simulation corresponds to
  ;time-period-to-simulate ;(input) the time period in seconds to simulate
  ;sim-time-buffer ;(input) the time period in seconds to start the simulation before the actual given 'time-period-to-simulate' and end later
  ;output-filename ;(input) the filename and path in which to save the output
  ;num-agents ;(input) the total number of agents entering the simulation each day
  ;car-travel-time ;(input) the time in seconds a car needs to travel from the satellite to the central parking and the other way round
  ;public-transport-delay ;(input) the time in seconds which the public transport is slower/faster than the cars when traveling from the satellite to the central parking and the other way round
  ;public-transport-interval ;(input) the frequency/time interval in seconds of public transportation departures
  ;public-transport-fee ;(input) the public transport fee for passengers
  ;regular-parking-fee ;(input) the parking fee at the central parking for regular parking spaces
  ;rideshare-parking-fee ;(input) the parking fee at the central parking for rideshare parking spaces
  ;driving-costs ;(input) the driving costs (oneway) for e.g gas, wear
  ;num-parking-spaces-satellite ;(input) the capacity/number of parking spaces at the satellite parking
  ;num-regular-parking-spaces-city ;(input) the capacity/number of regular parking spaces at the central parking
  ;num-ridesharing-parking-spaces-city ;(input) the capacity/number of parking spaces at the central parking reserved for ridesharing vehicles
  ;willingness-mean ;(input) the normal distributions mean describing the willingess for rideshare driving
  ;willingness-standard-deviation ;(input) the normal distributions standard deviation describing the willingess for rideshare driving
  ;value-of-time-mean ;(input) the normal distributions mean describing the "value of time"
  ;value-of-time-standard-deviation ;(input) the normal distributions standard deviation describing the "value of time"
  ;time-in-city-alpha ;(input) the gamma distributions alpha describing the time spent in the city
  ;time-in-city-lamda ;(input) the gamma distributions lamda describing the time spent in the city
  ;max-num-passenger-seats-car ;(input) the maximum number of passenger seats of a car
  ;max-num-passenger-seats-public-transport ;(input) the maximum number of passenger seats of a public transportation vehicle
  ;mean-update-if-no-change ;(input) the time in seconds after which the mean values will be decreased if no update happend
  ;central-parking-mode ;(chooser) the mode how central parking lots are managed:
                        ;"split" means there are dedicated parking lots for ridesharing (which can only used by rideshare drivers) and regular parking lots which can be used by everyone
                        ;"shared" means that there are no special dedicated parking lots for rideshare drivers, parking lots can be used by everyone
                        ;in both cases rideshare drivers are preferred over solo drivers
  ;time-parking-in-empty-parking-garage ;(input) the time (in senconds) needed to park a car in a empty parking garage
  ;max-time-parking ;(input) the maximum time (in senconds) needed to park a car in a parking garage
  ;satellite-parking-time ;(input) the time (in senconds) needed to park a car at the satellite parking

  new-simulation? ;boolean indicating whether a new simulation started or not
  stop? ;boolean indicating whether to stop the simulation or not
  output-file ;the filename and path in which to save the output
  seperator ;the delimiter to use in the output file for seperating the values

  num-iterations ;the current iteration number
  total-num-iterations ;the total number of iterations to run
  num-simulation ;the current simulation number
  total-num-simulations ;the total number of simulations to run

  ticks-to-start ;the number of ticks passed since 0:00 of day 1 before starting the simulation
  sim-start-tick ;the tick of the simulations start (excluding the sim-time-buffer)
  sim-end-tick  ;the tick of the simulations end (excluding the sim-time-buffer)

  sim-datetime ;a string containing the date and time of the simulation
  sim-day ;the day within the simulation
  sim-hour ;the hour within the simulation
  sim-minute ;the minute within the simulation
  sim-second ;the second within the simulation

  num-spawn-turtles-list ;the number of agents to spawn simultaneously in each time interval
  num-free-parking-spaces-satellite ;the current number of free parking spaces at the satellite parking
  num-free-regular-parking-spaces-city ;the current number of free regular parking spaces at the central parking
  num-free-ridesharing-parking-spaces-city ;the current number of free parking spaces at the central parking reserved for ridesharing vehicles

  mean-waiting-time-pickup-to-city ;the mean/average waiting time for getting picked up the central parking
  mean-waiting-time-pickup-to-satellite ;the mean/average waiting time for getting picked up the satellite parking
  mean-solo-driver-waiting-for-parking-lot ;the mean/average waiting time for a solo drivers finding/getting a parking lot
  mean-rideshare-driver-waiting-for-parking-lot ;the mean/average waiting time for a rideshare drivers finding/getting a parking lot
]
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; agent classes section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
breed [rideshare-drivers rideshare-driver]
breed [rideshare-passengers rideshare-passenger]
breed [solo-drivers solo-driver]
breed [public-transport-passengers public-transport-passenger]
breed [public-transports public-transport]
breed [dummy-parking-cars dummy-parking-car]

turtles-own [
  no-class? ;boolean indicating whether a class was assigned to the agent or not
  need-action? ;boolean indicating whether the agent has permormed an action the round/tick or not
  status ;the status of the agent, see each class for its possible values
  value-of-time ;number indicating how valuable time (1h) is for the agent
  willingness ;number indicating the willingness for ridesharing for the agent

  timestamp-start ;the timestamp the agent started his trip
  timestamp-end ;the timestamp the agent finished his trip
  time-start ;the time in seconds the agent started his trip
  time-end ;the time in seconds the agent finished his trip
  type_to_central_parking ;the agents travel type to the central parking, one of {rideshare-drivers, rideshare-passengers, solo-drivers, public-transport-passengers}
  type_to_satellite_parking ;the agents travel type to the satellite parking, one of {rideshare-drivers, rideshare-passengers, solo-drivers, public-transport-passengers}
  used-parking-space ;the used parking space class, one of {satellite, central-regular, central-ridesharing}
  time_on_road_to_central_parking ;the time in seconds the agent spent on the way/road to the central parking
  time_on_road_to_satellite_parking ;the time in seconds the agent spent on the way/road to the satellite parking
  time-spent-in-city ;the time in sencods the agent spent in the city
  money-spent ;the money (sum) an agent spent on the way to the city center and on the way back
]

rideshare-drivers-own [
  ;status ;one of {pickup-passengers-to-city, traveling-to-city, waiting-free-parking-space, parking-car, doing-stuff-in-city, pickup-passengers-to-satellite, traveling-to-satellite, heading-home}
  time-spent-at-satellite-parking ;the time/number of ticks the agent is/was waiting at the satellite parking for pickup
  time-spent-at-central-parking ;the time/number of ticks the agent is/was waiting at the central parking for pickup
  counter-doing-stuff-in-city ;the time/number of ticks the agent spends in the city
  passenger-seats-left ;the number of free passenger seats of the agent
  passenger-list ;a list of passengers of the agent
  counter-waiting-free-parking-space ;the time/number of ticks the agent waits for a free parking space at the central parking
  counter-finding-free-parking-space ;the time/number of ticks needed to find a free parking lot at the central parking (inside the garage)
  parking-time ;the time (in seconds) needed to park the car at the central parking
]

rideshare-passengers-own [
  ;status ;one of {parking-car, waiting-pickup-to-city, traveling-to-city, doing-stuff-in-city, waiting-pickup-to-satellite, traveling-to-satellite, heading-home}
  time-spent-at-satellite-parking ;the time/number of ticks the agent is/was waiting at the satellite parking for pickup
  time-spent-at-central-parking ;the time/number of ticks the agent is/was waiting at the central parking for pickup
  counter-doing-stuff-in-city ;the time/number of ticks the agent spends in the city
  car-dummy ;the dummy car at the satellite parking belonging to this agent
  counter-parking-car ;the time (in senconds) needed to park the car at the satellite parking
]

solo-drivers-own [
  ;status ;one of {traveling-to-city, parking-car, waiting-free-parking-space, doing-stuff-in-city, traveling-to-satellite, heading-home}
  time-spent-at-satellite-parking ;the time/number of ticks the agent is/was waiting at the satellite parking for pickup
  time-spent-at-central-parking ;the time/number of ticks the agent is/was waiting at the central parking for pickup
  counter-doing-stuff-in-city ;the time/number of ticks the agent spends in the city
  counter-waiting-free-parking-space ;the time/number of ticks the agent waits for a free parking space at the central parking
  counter-finding-free-parking-space ;the time/number of ticks needed to find a free parking lot at the central parking (inside the garage)
  parking-time ;the time (in seconds) needed to park the car at the central parking
]

public-transport-passengers-own [
  ;status ;one of {parking-car, waiting-pickup-to-city, traveling-to-city, doing-stuff-in-city, waiting-pickup-to-satellite, traveling-to-satellite, heading-home}
  time-spent-at-satellite-parking ;the time/number of ticks the agent is/was waiting at the satellite parking for pickup
  time-spent-at-central-parking ;the time/number of ticks the agent is/was waiting at the central parking for pickup
  counter-doing-stuff-in-city ;the time/number of ticks the agent spends in the city
  car-dummy ;the dummy car at the satellite parking belonging to this agent
  counter-parking-car ;the time (in senconds) needed to park the car at the satellite parking
]

public-transports-own [
  ;status ;one of {pickup-passengers-to-city, traveling-to-city, pickup-passengers-to-satellite, traveling-to-satellite}
  passenger-seats-left ;the number of free passenger seats of the agent
  passenger-list ;a list of passengers of the agent
]

dummy-parking-cars-own [
  ;status ;one of {parking}
  owner ;the agent owning the car
]
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; multiple specified simulations section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
; function for running multiple simulations with a given parameter setting one after another without user interaction
to run-all-simulations
  ;the simulations to run - order of parameters:
  let simulation_default [
    true                       ;display?
    true                       ;ridesharing?
    30                         ;update-interval
    86400                      ;time-period-to-simulate
    21600                      ;sim-time-buffer
    "output\\SIMULATION"       ;output-filename
    4000                       ;num-agents
    1800                       ;car-travel-time
    900                        ;public-transport-delay
    1800                       ;public-transport-interval
    3                          ;public-transport-fee
    10                         ;regular-parking-fee
    5                          ;rideshare-parking-fee
    4                          ;driving-costs
    5000                       ;num-parking-spaces-satellite
    400                        ;num-regular-parking-spaces-city
    0                          ;num-ridesharing-parking-spaces-city
    5                          ;willingness-mean
    5                          ;willingness-standard-deviation
    25                         ;value-of-time-mean
    10                         ;value-of-time-standard-deviation
    3                          ;time-in-city-alpha
    2                          ;time-in-city-lamda
    3                          ;max-num-passenger-seats-car
    50                         ;max-num-passenger-seats-public-transport
    630                        ;mean-update-if-no-change
    "shared"                   ;central-parking-mode
    60                         ;time-parking-in-empty-parking-garage
    300                        ;max-time-parking
    60                         ;satellite-parking-time
  ]

  ;baseline simulation
  ;let simulation_0 [false false 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_0 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'num-agents'
  let simulation_1 [false true 30 86400 21600  "output\\SIMULATION" 1000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_2 [false true 30 86400 21600  "output\\SIMULATION" 1500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_3 [false true 30 86400 21600  "output\\SIMULATION" 2000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_4 [false true 30 86400 21600  "output\\SIMULATION" 2500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_5 [false true 30 86400 21600  "output\\SIMULATION" 3000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_6 [false true 30 86400 21600  "output\\SIMULATION" 3500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_7 [false true 30 86400 21600  "output\\SIMULATION" 4500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_8 [false true 30 86400 21600  "output\\SIMULATION" 5000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_9 [false true 30 86400 21600  "output\\SIMULATION" 5500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_10 [false true 30 86400 21600  "output\\SIMULATION" 6000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_11 [false true 30 86400 21600  "output\\SIMULATION" 6500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_12 [false true 30 86400 21600  "output\\SIMULATION" 7000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_13 [false true 30 86400 21600  "output\\SIMULATION" 7500 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_14 [false true 30 86400 21600  "output\\SIMULATION" 8000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'public-transport-delay'
  let simulation_15 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 390 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_16 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 810 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_17 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 990 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_18 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 1410 1800 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'public-transport-interval'
  let simulation_19 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 600 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_20 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1200 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_21 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 2400 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_22 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 3000 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_23 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 3600 3 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'public-transport-fee'
  let simulation_24 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 1 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_25 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 2 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_26 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 4 10 5 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'rideshare-parking-fee'
  let simulation_27 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 3 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_28 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 4 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_29 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 6 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_30 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 10 4 5000 400 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'num-regular-parking-spaces-city'
  let simulation_31 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 350 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_32 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 450 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_33 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 500 0 5 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'willingness-mean'
  let simulation_34 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 3 5 25 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_35 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 7 5 25 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'value-of-time-mean'
  let simulation_36 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 15 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_37 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 20 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_38 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 30 10 3 2 3 50 630 "shared" 60 300 60]
  let simulation_39 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 35 10 3 2 3 50 630 "shared" 60 300 60]

  ;vary 'time-in-city-alpha' and 'time-in-city-lamda'
  let simulation_40 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 2 2 3 50 630 "shared" 60 300 60]
  let simulation_41 [false true 30 86400 21600  "output\\SIMULATION" 4000 1800 900 1800 3 10 5 4 5000 400 0 5 5 25 10 2 1 3 50 630 "shared" 60 300 60]

  ;list with ridesharing
  let simulation-list-1 (list
    simulation_0
    simulation_1  simulation_2  simulation_3  simulation_4  simulation_5  simulation_6  simulation_7  simulation_8  simulation_9  simulation_10
    simulation_11 simulation_12 simulation_13 simulation_14 simulation_15 simulation_16 simulation_17 simulation_18 simulation_19 simulation_20
    simulation_21 simulation_22 simulation_23 simulation_24 simulation_25 simulation_26 simulation_27 simulation_28 simulation_29 simulation_30
    simulation_31 simulation_32 simulation_33 simulation_34 simulation_35 simulation_36 simulation_37 simulation_38 simulation_39 simulation_40
    simulation_41
  )

  ;list without ridesharing
  let simulation-list-2 []
  foreach simulation-list-1 [
    simulation ->
    ;set simulation (replace-item 1 simulation false)
    set simulation-list-2 (lput (replace-item 1 simulation false) simulation-list-2)
  ]

  let simulation-list (sentence simulation-list-1 simulation-list-2)

  ; NOTE:
  ; to add more simulations just copy one of the given 'examples' above, modify the desired parameters, choose a simulation number which is not already taken and add the simulation to the 'simulation-list'
  ; to remove simulations just uncomment or delete the corresponding line and remove the simulation from the 'simulation-list'

  let num-iter 1
  let total-num-iter 100
  while [num-iter <= total-num-iter] [
    let num-sim 1
    let total-num-sim (length simulation-list)
    foreach simulation-list [
      simulation-parameters ->
      set-input-values simulation-parameters
      setup-simulation num-iter total-num-iter num-sim total-num-sim
      if new-simulation? [while [not stop?] [run-simulation]]
      set num-sim (num-sim + 1)
    ]
    set num-iter (num-iter + 1)
  ]
end
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; simulation setup section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
; main setup function
to setup-simulation [num-iter total-num-iter num-sim total-num-sim]
  clear-output ;TODO: clear command center also possible? > no?!
  output-print "1. Setting up simulation, please wait! ... "
  file-close-all
  clear-all
  no-display
  reset-ticks
  set new-simulation? false
  if (not check-input-values) [
    clear-ticks
    stop
  ]
  initialize-global-variables num-iter total-num-iter num-sim total-num-sim
  setup-patches
  setup-turtles
  setup-output-file
  if (display?) [
    display
  ]
  clear-output
  output-print "1. Setting up simulation, please wait! ... DONE! :)"
end

; function checking the input values
; returns true if all values are as expected, false otherwise
to-report check-input-values
  ;TODO for now very simple > more complex input validation?!
  let input-values (list time-period-to-simulate sim-time-buffer car-travel-time public-transport-delay public-transport-interval mean-update-if-no-change time-parking-in-empty-parking-garage max-time-parking satellite-parking-time)
  let input-value-variables ["time-period-to-simulate" "sim-time-buffer" "car-travel-time" "public-transport-delay" "public-transport-interval" "mean-update-if-no-change" "time-parking-in-empty-parking-garage" "max-time-parking" "satellite-parking-time"]
  let index 0
  foreach input-values [
    input-value ->
    ; check if the input values are integer multiples of the update-interval
    if ((input-value mod update-interval) != 0) [
      output-print (word "ERROR: " (item index input-value-variables) " (" input-value ") needs to be a integer multiple of the update-interval (" update-interval ")")  ;TODO: proper error handling
      report false
    ]
    ;check that mean-update-if-no-change is not a integer mupltiple of the public-transport-interval
    if (index = 4 and (input-value mod mean-update-if-no-change = 0)) [
      output-print (word "ERROR: " (item index input-value-variables) " (" input-value ") is not allowed to be a integer multiple of the mean-update-if-no-change (" mean-update-if-no-change ")")  ;TODO: proper error handling
      report false
    ]
    set index (index + 1)
  ]
  report true
end

; function for initializing the global variables
to initialize-global-variables [num-iter total-num-iter num-sim total-num-sim]
  set ticks-to-start ((86400 - (sim-time-buffer mod 86400)) / update-interval)
  set sim-start-tick (ticks-to-start + (sim-time-buffer / update-interval))
  set sim-end-tick (sim-start-tick + (time-period-to-simulate / update-interval))

  let tick-counter ticks-to-start
  while [(tick-counter != 0)] [
   tick
   set tick-counter (tick-counter - 1)
  ]

  set num-iterations num-iter
  set total-num-iterations total-num-iter
  set num-simulation num-sim
  set total-num-simulations total-num-sim

  set stop? false
  set new-simulation? true
  set seperator ";"
  set num-free-regular-parking-spaces-city num-regular-parking-spaces-city
  set num-free-ridesharing-parking-spaces-city num-ridesharing-parking-spaces-city

  update-sim-datetime

  set mean-waiting-time-pickup-to-city [0 0]
  set mean-waiting-time-pickup-to-satellite [0 0]
  set mean-solo-driver-waiting-for-parking-lot [0 0]
  set mean-rideshare-driver-waiting-for-parking-lot [0 0]

  ; generating a list holding the number of agents to spawn at each time step
  let traffic-distribution-list (n-values (86400 / update-interval) [i -> (traffic-distribution (i * update-interval))]) ;calculate the traffic-distribution for the list of all time steps for one day
  let float-num-spawn-turtles-list (map [x -> (num-agents * (1 / (sum traffic-distribution-list)) * x)] traffic-distribution-list) ;norm the list (sum = 1) and multipy with the total numer of agents so spawn
  let int-num-spawn-turtles-list (map floor float-num-spawn-turtles-list) ;extract the integer part of the values
  let decimal-num-spawn-turtles-list (map [x -> x mod 1] float-num-spawn-turtles-list)  ;extract the decimal part of the values
                                                                                        ; since spawning a fraction of a turtle is not possible, sum up the fractions to 0.5 and spawn at this time step a additional turtle, subtract 1 from the sum and continue...
  let index 0
  let current-sum 0
  foreach decimal-num-spawn-turtles-list [
    decimal-number ->
    set current-sum (current-sum + decimal-number)
    ifelse (current-sum >= 0.5) [
      set current-sum (current-sum - 1)
      set decimal-num-spawn-turtles-list (replace-item index decimal-num-spawn-turtles-list 1)
    ] [
      set decimal-num-spawn-turtles-list (replace-item index decimal-num-spawn-turtles-list 0)
    ]
    set index (index + 1)
  ]
  let num-spawn-turtles-list-one-day (map + int-num-spawn-turtles-list decimal-num-spawn-turtles-list)
  set num-spawn-turtles-list []

  let total-num-days (1 + (2 * (ceiling (sim-time-buffer / 86400))))
  while [(total-num-days != 0)] [
    set num-spawn-turtles-list (sentence num-spawn-turtles-list num-spawn-turtles-list-one-day)
    set total-num-days (total-num-days - 1)
  ]

  ;TODO: warn user to set this number big enough > for now: input wil be ignored and overwritten!
  ;set num-parking-spaces-satellite (num-agents / 2)
  set num-free-parking-spaces-satellite num-parking-spaces-satellite
end

; function for initializing the patches
to setup-patches
  ; setup the parking spaces
  ask patches with [pxcor <= -6 and pxcor >= -11 and pycor <= 5 and pycor >= -5] [set pcolor white]
  ask patches with [pxcor >= 6 and pxcor <= 11 and pycor <= 5 and pycor >= -5] [set pcolor white]
  ; setup the waiting areas for passengers
  ask patches with [pxcor = -6 and pycor = 2] [set pcolor yellow]
  ask patches with [pxcor = 6 and pycor = 4] [set pcolor yellow]
  ; setup the roads
  ask patches with [pxcor <= -12 and pxcor >= -14 and pycor = -1] [set pcolor grey]
  ask patches with [pxcor <= -12 and pxcor >= -14 and pycor = 1] [set pcolor grey]
  ask patches with [pxcor <= 5 and pxcor >= -5 and pycor <= 0 and pycor >= -1] [set pcolor grey]
  ask patches with [pxcor <= 5 and pxcor >= -5 and pycor <= -3 and pycor >= -4] [set pcolor grey]
  ; setup the public transportation roads
  ask patches with [pxcor <= 5 and pxcor >= -5 and pycor = 2] [set pcolor grey]
  ask patches with [pxcor <= 5 and pxcor >= -5 and pycor = 4] [set pcolor grey]

  if (ridesharing?) [
    ; setup the reserved ridesharing parking spaces
    ask patches with [pxcor <= 11 and pxcor >= 10 and pycor <= -4 and pycor >= -5] [set pcolor orange]
    ; setup the waiting areas for passengers
    ask patches with [pxcor <= -8 and pxcor >= -9 and pycor <= -4 and pycor >= -5] [set pcolor yellow]
    ask patches with [pxcor <= 9 and pxcor >= 8 and pycor <= -4 and pycor >= -5] [set pcolor yellow]
  ]
end

; function for initializing the agents
to setup-turtles
  set-default-shape turtles "car"
  set-default-shape solo-drivers "car"
  set-default-shape rideshare-drivers "car"
  set-default-shape rideshare-passengers "person"
  set-default-shape public-transport-passengers "person"
  set-default-shape public-transports "bus"
end

; function for initializing the output file
to setup-output-file
  ; get current date and time
  let dt-string date-and-time
  ; parse the date
  let date substring dt-string 16 27
  ; parse the time
  let hours substring dt-string 0 2
  if (substring dt-string 13 15 = "PM") [
    set hours read-from-string substring dt-string 0 2 + 12
  ]
  let time (word hours "-" substring dt-string 3 5 "-" substring dt-string 6 12)
  ; set the output file, format: NAME (DAY-MONTH-YEAR HOUR-MINUTE-SECOND.MILLISECOND)
  set output-file (word output-filename "-" num-simulation " (" date " " time ")")
end

; function for restoring the original/default values of the inputs
to restore-default-input-values
  let simulation_default [
    true                       ;display?
    true                       ;ridesharing?
    30                         ;update-interval
    86400                      ;time-period-to-simulate
    21600                      ;sim-time-buffer
    "output\\SIMULATION"       ;output-filename
    4000                       ;num-agents
    1800                       ;car-travel-time
    900                        ;public-transport-delay
    1800                       ;public-transport-interval
    3                          ;public-transport-fee
    10                         ;regular-parking-fee
    5                          ;rideshare-parking-fee
    4                          ;driving-costs
    5000                       ;num-parking-spaces-satellite
    400                        ;num-regular-parking-spaces-city
    0                          ;num-ridesharing-parking-spaces-city
    5                          ;willingness-mean
    5                          ;willingness-standard-deviation
    25                         ;value-of-time-mean
    10                         ;value-of-time-standard-deviation
    3                          ;time-in-city-alpha
    2                          ;time-in-city-lamda
    3                          ;max-num-passenger-seats-car
    50                         ;max-num-passenger-seats-public-transport
    630                        ;mean-update-if-no-change
    "shared"                   ;central-parking-mode
    60                         ;time-parking-in-empty-parking-garage
    300                        ;max-time-parking
    60                         ;satellite-parking-time
  ]
  set-input-values simulation_default

  ;TODO: Unused at the moment
  set satellite-parking-fee 0
end

; function for setting the the input parameters programmatically
to set-input-values [arguments]
  ifelse (length arguments = 30) [
    set display? (item 0 arguments)
    set ridesharing? (item 1 arguments)
    set update-interval (item 2 arguments)
    set time-period-to-simulate (item 3 arguments)
    set sim-time-buffer (item 4 arguments)
    set output-filename (item 5 arguments)
    set num-agents (item 6 arguments)
    set car-travel-time (item 7 arguments)
    set public-transport-delay (item 8 arguments)
    set public-transport-interval (item 9 arguments)
    set public-transport-fee (item 10 arguments)
    set regular-parking-fee (item 11 arguments)
    set rideshare-parking-fee (item 12 arguments)
    set driving-costs (item 13 arguments)
    set num-parking-spaces-satellite (item 14 arguments)
    set num-regular-parking-spaces-city (item 15 arguments)
    set num-ridesharing-parking-spaces-city (item 16 arguments)
    set willingness-mean (item 17 arguments)
    set willingness-standard-deviation (item 18 arguments)
    set value-of-time-mean (item 19 arguments)
    set value-of-time-standard-deviation (item 20 arguments)
    set time-in-city-alpha (item 21 arguments)
    set time-in-city-lamda (item 22 arguments)
    set max-num-passenger-seats-car (item 23 arguments)
    set max-num-passenger-seats-public-transport (item 24 arguments)
    set mean-update-if-no-change (item 25 arguments)
    set central-parking-mode (item 26 arguments)
    set time-parking-in-empty-parking-garage (item 27 arguments)
    set max-time-parking (item 28 arguments)
    set satellite-parking-time (item 29 arguments)
  ] [
    output-print "ERROR: number of arguments not correct!" ;TODO: proper error handling
  ]
end
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; simulation main logic section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
; main logic function, one iteration represents one 'time interval/step' of the simulation
to run-simulation
  no-display
  if (new-simulation?) [
    set new-simulation? false
    output-print (word "2. Running simulation (" num-iterations "/" total-num-iterations " | " num-simulation "/" total-num-simulations "), please wait! ... \n")
    file-open output-file

    let input-parameter-list (list
      display?
      ridesharing?
      update-interval
      time-period-to-simulate
      sim-time-buffer
      output-filename
      num-agents
      car-travel-time
      public-transport-delay
      public-transport-interval
      public-transport-fee
      regular-parking-fee
      rideshare-parking-fee
      driving-costs
      num-parking-spaces-satellite
      num-regular-parking-spaces-city
      num-ridesharing-parking-spaces-city
      willingness-mean
      willingness-standard-deviation
      value-of-time-mean
      value-of-time-standard-deviation
      time-in-city-alpha
      time-in-city-lamda
      max-num-passenger-seats-car
      max-num-passenger-seats-public-transport
      mean-update-if-no-change
      central-parking-mode
      time-parking-in-empty-parking-garage
      max-time-parking
      satellite-parking-time
    )

    write-output (word "Parameters: ["
      "display?" " "
      "ridesharing?" " "
      "update-interval" " "
      "time-period-to-simulate" " "
      "sim-time-buffer" " "
      "output-filename" " "
      "num-agents" " "
      "car-travel-time" " "
      "public-transport-delay" " "
      "public-transport-interval" " "
      "public-transport-fee" " "
      "regular-parking-fee" " "
      "rideshare-parking-fee" " "
      "driving-costs" " "
      "num-parking-spaces-satellite" " "
      "num-regular-parking-spaces-city" " "
      "num-ridesharing-parking-spaces-city" " "
      "willingness-mean" " "
      "willingness-standard-deviation" " "
      "value-of-time-mean" " "
      "value-of-time-standard-deviation" " "
      "time-in-city-alpha" " "
      "time-in-city-lamda" " "
      "max-num-passenger-seats-car" " "
      "max-num-passenger-seats-public-transport" " "
      "mean-update-if-no-change" " "
      "central-parking-mode" " "
      "time-parking-in-empty-parking-garage" " "
      "max-time-parking" " "
      "satellite-parking-time]"
    )

    write-output (word "Parameters: " input-parameter-list)     ;TODO: check if list complete, all needed and right order!
    write-output "-------------------NEW SIMULATION-------------------"
    write-output (word
      "0" seperator
      "id" seperator
      "type_to_central_parking" seperator
      "type_to_satellite_parking" seperator
      "used-parking-space" seperator
      "total-time" seperator
      "time-start" seperator
      "time-end" seperator
      "timestamp-start" seperator
      "timestamp-end" seperator
      "waiting_time_at_satellite_parking" seperator
      "waiting_time_at_central_parking" seperator
      "waiting-time-free-parking-space" seperator
      "time-parking-car-at-central-parking" seperator
      "time_on_road_to_central_parking" seperator
      "time_on_road_to_satellite_parking" seperator
      "time-spent-in-city" seperator
      "value-of-time" seperator
      "willingness" seperator
      "money-spent"
    )
    write-output (word
      "1" seperator
      "current-time" seperator
      "num-parking-spaces-satellite" seperator
      "num-free-parking-spaces-satellite" seperator
      "num-regular-parking-spaces-city" seperator
      "num-free-regular-parking-spaces-city" seperator
      "num-ridesharing-parking-spaces-city" seperator
      "num-free-ridesharing-parking-spaces-city" seperator
      "num-rs-waiting-pickup-to-city" seperator
      "num-rs-waiting-pickup-to-satellite" seperator
      "num-pt-waiting-pickup-to-city" seperator
      "num-pt-waiting-pickup-to-satellite" seperator
      "mean-waiting-time-pickup-to-city" seperator
      "mean-waiting-time-pickup-to-satellite" seperator
      "mean-solo-driver-waiting-for-parking-lot" seperator
      "mean-rideshare-driver-waiting-for-parking-lot"
    )

    reset-timer
  ]

  update-sim-datetime

  create-turtle-agents (item ticks num-spawn-turtles-list)
  ask turtles [set need-action? true] ;reset the action falg so in this new round every turtle can perform an new action

  process-turtles
  if (ridesharing?) [
    process-rideshare-drivers
    process-rideshare-passengers
  ]
  process-solo-drivers
  process-parking
  process-public-transport
  process-public-transport-passengers

  update-means-if-no-change

  if ((ticks >= sim-start-tick) and (ticks <= sim-end-tick)) [
    write-output (word
      "1" seperator ;since two different statistics are written to the same output file, this flag indicating which one, so lines can be filtered
      (ticks * update-interval) seperator
      num-parking-spaces-satellite seperator
      num-free-parking-spaces-satellite seperator
      num-regular-parking-spaces-city seperator
      num-free-regular-parking-spaces-city seperator
      num-ridesharing-parking-spaces-city seperator
      num-free-ridesharing-parking-spaces-city seperator
      (count rideshare-passengers with [status = "waiting-pickup-to-city"]) seperator
      (count rideshare-passengers with [status = "waiting-pickup-to-satellite"]) seperator
      (count public-transport-passengers with [status = "waiting-pickup-to-city"]) seperator
      (count public-transport-passengers with [status = "waiting-pickup-to-satellite"]) seperator
      (item 0 mean-waiting-time-pickup-to-city) seperator
      (item 0 mean-waiting-time-pickup-to-satellite) seperator
      (item 0 mean-solo-driver-waiting-for-parking-lot) seperator
      (item 0 mean-rideshare-driver-waiting-for-parking-lot)
    )
  ]

  if ((stop?) or (ticks >= (ticks-to-start + ceiling ((time-period-to-simulate + (2 * sim-time-buffer)) / update-interval) - 1))) [
    set stop? true
    write-output "----------------------------------------------------"
    write-output (word "Time Taken: " timer " seconds")
    output-print (word "\nSimulation finished/stopped!") ;TODO: show mesage
    file-close
    clear-ticks
    stop
  ]

  if (display?) [
    display
  ]

  tick
end

; function to stop the simulation after the current round
to stop-simulation
  set stop? true
end

; function to create the given number of new agents with the specified attributes
to create-turtle-agents [num-turtles]
  create-turtles num-turtles [
    set color blue
    set heading 90
    set xcor -15
    set ycor -1
    set value-of-time ((random-normal value-of-time-mean value-of-time-standard-deviation) / 3600) ;the agents value of time for a second
    if (value-of-time < 0) [set value-of-time 0] ;value of time can't be negative
    set willingness (random-normal willingness-mean willingness-standard-deviation) ;the willingness for ridesharing
    set no-class? true
    set need-action? true
  ]
end

; function for processing all new agents and agents leaving the system
to process-turtles
  ; processing all new agents without any class
  ask turtles with [no-class? and need-action?] [
    fd random-in-range 0.5 1
    ; let agents make a decision what to become based on the specified cost functions
    if ((xcor > -12) and (ycor = -1)) [
      set timestamp-start sim-datetime
      set time-start ticks

      ifelse (ridesharing?) [
        let cost-list (sublist (calculate-cost-functions "not-specified") 0 4)
        let new-class item (position (min cost-list) cost-list) ["rideshare-driver" "rideshare-passenger" "solo-driver" "public-transport-passenger"]
        set-new-class new-class
      ] [
        let cost-list (sublist (calculate-cost-functions "not-specified") 2 4)
        let new-class item (position (min cost-list) cost-list) ["solo-driver" "public-transport-passenger"]
        set-new-class new-class
      ]

      if ((is-rideshare-passenger? self) or (is-public-transport-passenger? self)) [
        set status "parking-car"
        set counter-parking-car (satellite-parking-time - update-interval) ;subtracting one update interval so one parking event will already performed this round
      ]

      set no-class? false
    ]
    set need-action? false
  ]

  ; processing all agents leaving the system ("heading-home")
  ask turtles with [status = "heading-home" and need-action?] [
    fd random-in-range 0.5 1
    if ((xcor < -14) and (ycor = 1)) [
      die
    ]
    set need-action? false
  ]
end

; function for processing the rideshare drivers
to process-rideshare-drivers
  ; processing all rideshare drivers with status "pickup-passengers-to-city"
  let list-passengers-waiting-pickup-to-city sort-on [(- time-spent-at-satellite-parking)] rideshare-passengers with [status = "waiting-pickup-to-city" and need-action?]
  let list-drivers-pickup-passengers-to-city sort-on [(- time-spent-at-satellite-parking)] rideshare-drivers with [status = "pickup-passengers-to-city" and need-action?]
  let waiting-time-list-to-city []

  foreach list-drivers-pickup-passengers-to-city [
    driver -> ask driver [
      foreach list-passengers-waiting-pickup-to-city [
        passenger ->
        ; check if the driver has seats left and pick up a person
        if (passenger-seats-left > 0) [
          set passenger-seats-left (passenger-seats-left - 1)
          set passenger-list (lput passenger passenger-list)
          ask passenger [
            set status "traveling-to-city"
            ;set hidden? true
            set xcor -15
            set ycor -9
            set type_to_central_parking breed
            set time_on_road_to_central_parking ticks
            set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
            set waiting-time-list-to-city (lput (time-spent-at-satellite-parking * update-interval) waiting-time-list-to-city)
            set need-action? false
          ]
          set list-passengers-waiting-pickup-to-city (remove-item 0 list-passengers-waiting-pickup-to-city)
        ]
        ; TODO: else break out of inner foreach loop but not possible in NetLogo?! so unecessary "emtpy" loops will be run?!
      ]

      ; check if the driver picked up any person or not
      ifelse (passenger-seats-left = max-num-passenger-seats-car) [
        ; check second best option next to be a rideshare-driver (cause there is no passenger which was/could be picked up)
        let cost-list (sublist (calculate-cost-functions "not-specified") 1 4)
        let new-class (item (position (min cost-list) cost-list) ["rideshare-passenger" "solo-driver" "public-transport-passenger"])
        set-new-class new-class
      ] [
        set status "traveling-to-city"
        set xcor -5
        set ycor random-in-range -4 -3
        set type_to_central_parking breed
        set time_on_road_to_central_parking ticks
        set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
        fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go
      ]
      set need-action? false
    ]
  ]

  ; update the mean waiting time for getting picked up to the central parking as rideshare passenger
  if (length waiting-time-list-to-city) > 0 [
    set mean-waiting-time-pickup-to-city (list (calculate-weighted-mean (mean waiting-time-list-to-city) (item 0 mean-waiting-time-pickup-to-city)) ticks)
  ]

  ; processing all rideshare drivers with status "traveling-to-city"
  ask rideshare-drivers with [status = "traveling-to-city" and need-action?] [
    fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go

    ; if the driver arrived at the central parking
    if ((xcor > 5) and ((ycor = -3) or (ycor = -4)))  [
      let money ((driving-costs + rideshare-parking-fee ) / ((length passenger-list) + 1))
      foreach passenger-list [
        passenger -> ask passenger [
          set status "doing-stuff-in-city"
          ;set hidden? true
          set xcor 15
          set ycor -9
          set time_on_road_to_central_parking ((ticks - time_on_road_to_central_parking) * update-interval)
          set counter-doing-stuff-in-city ((random-gamma time-in-city-alpha time-in-city-lamda) * 3600)
          set time-spent-in-city ((ceiling (counter-doing-stuff-in-city / update-interval)) * update-interval)
          set money-spent (money-spent + money)
          set need-action? false
        ]
      ]
      set passenger-list []
      set passenger-seats-left max-num-passenger-seats-car
      set time_on_road_to_central_parking ((ticks - time_on_road_to_central_parking) * update-interval)
      set money-spent (money-spent + money)
      set status "waiting-free-parking-space"
      set xcor 7
      set ycor -5
    ]
    set need-action? false
  ]

  ; processing all rideshare drivers with status "parking-car"
  let waiting-time-list-rideshare-driver []
  ask rideshare-drivers with [status = "parking-car" and need-action?] [
    ; if the driver is done with parking his car
    ifelse (counter-finding-free-parking-space <= 0) [
      set status "doing-stuff-in-city"
      set counter-doing-stuff-in-city ((random-gamma time-in-city-alpha time-in-city-lamda) * 3600)
      set time-spent-in-city ((ceiling (counter-doing-stuff-in-city / update-interval)) * update-interval)
      set waiting-time-list-rideshare-driver (lput ((counter-waiting-free-parking-space * update-interval) + parking-time) waiting-time-list-rideshare-driver)
    ][
      set counter-finding-free-parking-space (counter-finding-free-parking-space - update-interval)
    ]
    set need-action? false
  ]

  ; update the mean waiting time for finding/getting a free parking space as rideshare driver
  if ((length waiting-time-list-rideshare-driver) > 0) [
    set mean-rideshare-driver-waiting-for-parking-lot (list (calculate-weighted-mean (mean waiting-time-list-rideshare-driver) (item 0 mean-rideshare-driver-waiting-for-parking-lot)) ticks)
  ]

  ; processing all rideshare drivers with status "doing-stuff-in-city"
  ask rideshare-drivers with [status = "doing-stuff-in-city" and need-action?] [
    ; if the driver is done with his duties in the city
    ifelse (counter-doing-stuff-in-city <= 0) [
      ifelse (used-parking-space = "central-ridesharing") [
        set num-free-ridesharing-parking-spaces-city (num-free-ridesharing-parking-spaces-city + 1)
      ] [
        set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city + 1)
      ]
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
      set status "pickup-passengers-to-satellite"
      set heading 270
      set xcor 8
      set ycor -3
    ] [
      set counter-doing-stuff-in-city (counter-doing-stuff-in-city - update-interval)
    ]
    set need-action? false
  ]

  ; processing all rideshare drivers with status "pickup-passengers-to-satellite"
  let list-passengers-waiting-pickup-to-satellite sort-on [(- time-spent-at-central-parking)] rideshare-passengers with [status = "waiting-pickup-to-satellite" and need-action?]
  let list-drivers-pickup-passengers-to-satellite sort rideshare-drivers with [status = "pickup-passengers-to-satellite" and need-action?]
  let waiting-time-list-to-satellite []

  foreach list-drivers-pickup-passengers-to-satellite [
    driver -> ask driver [
      foreach list-passengers-waiting-pickup-to-satellite [
        passenger ->
        ; check if the driver has seats left and pick up a person
        if (passenger-seats-left > 0) [
          set passenger-seats-left (passenger-seats-left - 1)
          set passenger-list (lput passenger passenger-list)
          ask passenger [
            set status "traveling-to-satellite"
            ;set hidden? true
            set xcor 15
            set ycor -9
            set type_to_satellite_parking breed
            set time_on_road_to_satellite_parking ticks
            set time-spent-at-central-parking (time-spent-at-central-parking + 1)
            set waiting-time-list-to-satellite (lput (time-spent-at-central-parking * update-interval) waiting-time-list-to-satellite)
            set need-action? false
          ]
          set list-passengers-waiting-pickup-to-satellite remove-item 0 list-passengers-waiting-pickup-to-satellite
        ]
        ; TODO: else break out of inner foreach loop but not possible in NetLogo?! so unecessary "emtpy" loops will be run?!
      ]

      ; check if the driver picked up any person or not
      if passenger-seats-left = max-num-passenger-seats-car [
        set breed solo-drivers
        set color blue
      ]

      set status "traveling-to-satellite"
      set xcor 5
      set ycor random-in-range -1 0
      set type_to_satellite_parking breed
      set time_on_road_to_satellite_parking ticks
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
      fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go
      set need-action? false
    ]
  ]

  ; update the mean waiting time for getting picked up to the satellite parking as rideshare passenger
  if (length waiting-time-list-to-satellite) > 0 [
    set mean-waiting-time-pickup-to-satellite (list (calculate-weighted-mean (mean waiting-time-list-to-satellite) (item 0 mean-waiting-time-pickup-to-satellite)) ticks)

  ]

  ; processing all rideshare drivers with status "traveling-to-satellite"
  ask rideshare-drivers with [status = "traveling-to-satellite" and need-action?] [
    fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go.

    ; if the driver arrived at the satellite parking
    if ((xcor < -5) and ((ycor = -1) or (ycor = 0)))  [
      let money (driving-costs / ((length passenger-list) + 1))
      foreach passenger-list [
        passenger -> ask passenger [
          set status "heading-home"
          ;set hidden? false
          set color blue
          set xcor -12
          set ycor 1
          ask car-dummy [die]
          set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite + 1)
          set time_on_road_to_satellite_parking ((ticks - time_on_road_to_satellite_parking) * update-interval)
          set money-spent (money-spent + money)
          set timestamp-end sim-datetime
          set time-end ticks
          write-statistics
          set breed turtles
          set need-action? false
        ]
      ]
      set passenger-list []
      set passenger-seats-left max-num-passenger-seats-car
      set time_on_road_to_satellite_parking ((ticks - time_on_road_to_satellite_parking) * update-interval)
      set money-spent (money-spent + money)
      set timestamp-end sim-datetime
      set time-end ticks
      set status "heading-home"
      set color blue
      set xcor -12
      set ycor 1
      write-statistics
      set breed turtles
    ]
    set need-action? false
  ]
end

; function for processing the rideshare passengers
to process-rideshare-passengers
  ; processing all rideshare passengers with status "parking-car"
  ask rideshare-passengers with [status = "parking-car" and need-action?] [
    ifelse (counter-parking-car <= 0) [
      set status "waiting-pickup-to-city"
    ] [
      set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
      set counter-parking-car (counter-parking-car - update-interval)
    ]
    set need-action? false
  ]

  ; processing all rideshare passengers with status "waiting-pickup-to-city"
  ask rideshare-passengers with [status = "waiting-pickup-to-city" and need-action?] [
    ; check if another option is more cost effcient than beeing a rideshare passenger
    let cost-list (sublist (calculate-cost-functions "rideshare-passenger") 0 4)
    let new-class (item (position (min cost-list) cost-list) ["rideshare-driver" "rideshare-passenger" "solo-driver" "public-transport-passenger"])
    ; check if to change the class or not
    ifelse (new-class != "rideshare-passenger") [
      if ((time-spent-at-satellite-parking * update-interval) > (item 0 mean-waiting-time-pickup-to-city)) [
        set mean-waiting-time-pickup-to-city (list (calculate-weighted-mean (time-spent-at-satellite-parking * update-interval) (item 0 mean-waiting-time-pickup-to-city)) ticks)
      ]
      set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite + 1)
      set-new-class new-class
    ] [
      set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
    ]
    set need-action? false
  ]

  ; processing all rideshare passengers with status "doing-stuff-in-city"
  ask rideshare-passengers with [status = "doing-stuff-in-city" and need-action?] [
    ; if the passenger is done with his duties in the city
    ifelse (counter-doing-stuff-in-city <= 0) [
      ; check if another option is more cost effcient than beeing a rideshare passenger
      let cost-list (sublist (calculate-cost-functions "rideshare-passenger") 4 6) ;(list costs-rideshare-driver costs-rideshare-passenger costs-solo-driver costs-public-transport-passenger)
      let new-class (item (position (min cost-list) cost-list) ["rideshare-passenger" "public-transport-passenger"])
      set-new-class-way-back new-class
    ] [
      set counter-doing-stuff-in-city (counter-doing-stuff-in-city - update-interval)
    ]
    set need-action? false
  ]

  ; processing all rideshare passengers with status "waiting-pickup-to-satellite"
  ask rideshare-passengers with [status = "waiting-pickup-to-satellite" and need-action?] [
    ; check if another option is more cost effcient than beeing a rideshare passenger
    let cost-list (sublist (calculate-cost-functions "rideshare-passenger") 4 6)
    let new-class (item (position (min cost-list) cost-list) ["rideshare-passenger" "public-transport-passenger"])
    ; check if to change the class or not
    ifelse (new-class != "rideshare-passenger") [
      if ((time-spent-at-central-parking * update-interval) > (item 0 mean-waiting-time-pickup-to-satellite)) [
        set mean-waiting-time-pickup-to-satellite (list (calculate-weighted-mean (time-spent-at-central-parking * update-interval) (item 0 mean-waiting-time-pickup-to-satellite)) ticks)
      ]
      set-new-class-way-back new-class
    ] [
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
    ]
    set need-action? false
  ]
end

; function for processing the solo drivers
to process-solo-drivers
  ; processing all solo drivers with status "traveling-to-city"
  ask solo-drivers with [status = "traveling-to-city" and need-action?] [
    fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go

    ; if the driver arrived at the central parking
    if ((xcor > 5) and ((ycor = -3) or (ycor = -4)))  [
      set status "waiting-free-parking-space"
      set xcor 7
      set ycor -5
      set time_on_road_to_central_parking ((ticks - time_on_road_to_central_parking) * update-interval)
      set money-spent (money-spent + driving-costs + regular-parking-fee)
    ]
    set need-action? false
  ]

  ; processing all solo drivers with status "parking-car"
  let waiting-time-list-solo-driver []
  ask solo-drivers with [status = "parking-car" and need-action?] [
    ; if the driver is done with parking his car
    ifelse (counter-finding-free-parking-space <= 0) [
      set status "doing-stuff-in-city"
      set counter-doing-stuff-in-city ((random-gamma time-in-city-alpha time-in-city-lamda) * 3600)
      set time-spent-in-city ((ceiling (counter-doing-stuff-in-city / update-interval)) * update-interval)
      set waiting-time-list-solo-driver (lput ((counter-waiting-free-parking-space * update-interval) + parking-time) waiting-time-list-solo-driver)
    ][
      set counter-finding-free-parking-space (counter-finding-free-parking-space - update-interval)
    ]
    set need-action? false
  ]

  ; update the mean waiting time for finding/getting a free parking space as solo driver
  if ((length waiting-time-list-solo-driver) > 0) [
    set mean-solo-driver-waiting-for-parking-lot (list (calculate-weighted-mean (mean waiting-time-list-solo-driver) (item 0 mean-solo-driver-waiting-for-parking-lot)) ticks)
  ]

  ; processing all solo drivers with status "doing-stuff-in-city"
  ask solo-drivers with [status = "doing-stuff-in-city" and need-action?] [
    ; if the driver is done with his duties in the city
    ifelse (counter-doing-stuff-in-city <= 0) [
      set status "traveling-to-satellite"
      set heading 270
      set xcor 5
      set ycor random-in-range -1 0
      set type_to_satellite_parking breed
      set time_on_road_to_satellite_parking ticks
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
      set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city + 1)
      fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go
    ] [
      set counter-doing-stuff-in-city (counter-doing-stuff-in-city - update-interval)
    ]
    set need-action? false
  ]

  ; processing all solo drivers with status "traveling-to-satellite"
  ask solo-drivers with [status = "traveling-to-satellite" and need-action?] [
    fd ((10 * update-interval) / car-travel-time) ;long form: (1 / ((car-travel-time / 10) / update-interval)) ;div 10 cause 10 patches to go

    ; if the driver arrived at the satellite parking
    if ((xcor < -5) and ((ycor = -1) or (ycor = 0)))  [
      set status "heading-home"
      set color blue
      set xcor -12
      set ycor 1
      set timestamp-end sim-datetime
      set time-end ticks
      set time_on_road_to_satellite_parking ((ticks - time_on_road_to_satellite_parking) * update-interval)
      set money-spent (money-spent + driving-costs)
      write-statistics
      set breed turtles
    ]
    set need-action? false
  ]
end

; function for processing all drivers waiting/looking for a parking space at the central parking
to process-parking
  let waiting-rideshare-drivers sort-on [(- counter-waiting-free-parking-space)] rideshare-drivers with [status = "waiting-free-parking-space" and need-action?]
  let waiting-solo-drivers sort-on [(- counter-waiting-free-parking-space)] solo-drivers with [status = "waiting-free-parking-space" and need-action?]
  let list-drivers-waiting-free-parking-space sort-on [(- counter-waiting-free-parking-space)] (turtle-set waiting-rideshare-drivers waiting-solo-drivers)

  ; check which mode should be applied to the central parking system
  if (central-parking-mode = "split") [
    foreach list-drivers-waiting-free-parking-space [
      driver -> ask driver [
        ifelse (is-rideshare-driver? driver) [
          ; process rideshare drivers
          ; check which type of parking space is free
          ifelse (num-free-ridesharing-parking-spaces-city = 0) [
            ifelse (num-free-regular-parking-spaces-city = 0) [
              set counter-waiting-free-parking-space (counter-waiting-free-parking-space + 1)
            ] [
              set used-parking-space "central-regular"
              set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city - 1)
              set status "parking-car"
              set xcor random-in-range 10 11
              set ycor random-in-range -5 -4

              ifelse (num-free-regular-parking-spaces-city > 0) [
                set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - num-free-regular-parking-spaces-city) / num-regular-parking-spaces-city)))
              ] [
                set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - 0.1) / num-regular-parking-spaces-city)))
              ]

              if (parking-time > max-time-parking) [set parking-time max-time-parking]
              set counter-finding-free-parking-space (parking-time - update-interval) ; subtracting one update interval so one parking event will already performed this round
              set parking-time ((ceiling (parking-time / update-interval)) * update-interval)
            ]
          ] [
            set used-parking-space "central-ridesharing"
            set num-free-ridesharing-parking-spaces-city (num-free-ridesharing-parking-spaces-city - 1)
            set status "parking-car"
            set xcor random-in-range 10 11
            set ycor random-in-range -5 -4

            ifelse (num-free-ridesharing-parking-spaces-city > 0) [
              set parking-time  (time-parking-in-empty-parking-garage / (1 - ((num-ridesharing-parking-spaces-city - num-free-ridesharing-parking-spaces-city) / num-ridesharing-parking-spaces-city)))
            ][
              set parking-time  (time-parking-in-empty-parking-garage / (1 - ((num-ridesharing-parking-spaces-city - 0.1) / num-ridesharing-parking-spaces-city)))
            ]

            if (parking-time > max-time-parking) [set parking-time max-time-parking]
            set counter-finding-free-parking-space (parking-time - update-interval) ; subtracting one update interval so one parking event will already performed this round
            set parking-time ((ceiling (parking-time / update-interval)) * update-interval)
          ]
        ] [
          ; process solo drivers
          ; check if a parking space is free
          ifelse (num-free-regular-parking-spaces-city = 0) [
            set counter-waiting-free-parking-space (counter-waiting-free-parking-space + 1)
          ] [
            set used-parking-space "central-regular"
            set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city - 1)
            set status "parking-car"
            set xcor random-in-range 10 11
            set ycor random-in-range -3 5

            ifelse (num-free-regular-parking-spaces-city > 0) [
              set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - num-free-regular-parking-spaces-city) / num-regular-parking-spaces-city)))
            ][
              set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - 0.1) / num-regular-parking-spaces-city)))
            ]

            if (parking-time > max-time-parking) [set parking-time max-time-parking]
            set counter-finding-free-parking-space (parking-time - update-interval) ; subtracting one update interval so one parking event will already performed this round
            set parking-time ((ceiling (parking-time / update-interval)) * update-interval)
          ]
        ]
        set need-action? false
      ]
    ]
  ]

  if (central-parking-mode = "shared") [
    ; process rideshare drivers first
    foreach waiting-rideshare-drivers [
      driver -> ask driver [
        ; check if a parking space is free
        ifelse (num-free-regular-parking-spaces-city = 0) [
          set counter-waiting-free-parking-space (counter-waiting-free-parking-space + 1)
        ] [
          set used-parking-space "central-regular"
          set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city - 1)
          set status "parking-car"
          set xcor random-in-range 10 11
          set ycor random-in-range -5 -4

          ifelse (num-free-regular-parking-spaces-city > 0) [
            set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - num-free-regular-parking-spaces-city) / num-regular-parking-spaces-city)))
          ][
            set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - 0.1) / num-regular-parking-spaces-city)))
          ]

          if (parking-time > max-time-parking) [set parking-time max-time-parking]
          set counter-finding-free-parking-space (parking-time - update-interval) ; subtracting one update interval so one parking event will already performed this round
          set parking-time ((ceiling (parking-time / update-interval)) * update-interval)
        ]
        set need-action? false
      ]
    ]
    ; process solo drivers last
    foreach waiting-solo-drivers [
      driver -> ask driver [
        ; check if a parking space is free
        ifelse (num-free-regular-parking-spaces-city = 0) [
          set counter-waiting-free-parking-space (counter-waiting-free-parking-space + 1)
        ] [
          set used-parking-space "central-regular"
          set num-free-regular-parking-spaces-city (num-free-regular-parking-spaces-city - 1)
          set status "parking-car"
          set xcor random-in-range 10 11
          set ycor random-in-range -3 5

          ifelse (num-free-regular-parking-spaces-city > 0) [
            set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - num-free-regular-parking-spaces-city) / num-regular-parking-spaces-city)))
          ][
            set parking-time (time-parking-in-empty-parking-garage / (1 - ((num-regular-parking-spaces-city - 0.1) / num-regular-parking-spaces-city)))
          ]

          if (parking-time > max-time-parking) [set parking-time max-time-parking]
          set counter-finding-free-parking-space (parking-time - update-interval) ; subtracting one update interval so one parking event will already performed this round
          set parking-time ((ceiling (parking-time / update-interval)) * update-interval)
        ]
        set need-action? false
      ]
    ]
  ]
end

; function for processing the public transportation
to process-public-transport
  ; create new public transportation vehicles
  if (ticks * update-interval) mod public-transport-interval = 0 [
    create-public-transports 1 [
      set status "pickup-passengers-to-city"
      set color green
      set heading 90
      set xcor -5
      set ycor 2
      set passenger-list []
      set passenger-seats-left max-num-passenger-seats-public-transport
      set no-class? false
      set need-action? true
    ]
    create-public-transports 1 [
      set status "pickup-passengers-to-satellite"
      set color green
      set heading 270
      set xcor 5
      set ycor 4
      set passenger-list []
      set passenger-seats-left max-num-passenger-seats-public-transport
      set no-class? false
      set need-action? true
    ]
  ]

  ; processing the public transportation vehicles with status "pickup-passengers-to-city"
  ask public-transports with [status = "pickup-passengers-to-city" and need-action?] [
    let list-passengers-waiting-pickup-to-city sort-on [(- time-spent-at-satellite-parking)] public-transport-passengers with [status = "waiting-pickup-to-city" and need-action?]

    foreach list-passengers-waiting-pickup-to-city [
      passenger ->
      if passenger-seats-left > 0 [
        set passenger-seats-left (passenger-seats-left - 1)
        set passenger-list lput passenger passenger-list
        ask passenger [
          set status "traveling-to-city"
          ;set hidden? true
          set xcor -15
          set ycor -9
          set type_to_central_parking breed
          set time_on_road_to_central_parking ticks
          set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
          set need-action? false
        ]
        set list-passengers-waiting-pickup-to-city remove-item 0 list-passengers-waiting-pickup-to-city
      ]
    ]
    set status "traveling-to-city"
    fd ((10 * update-interval) / (car-travel-time + public-transport-delay))
    set need-action? false
  ]

  ; processing the public transportation vehicles with status "traveling-to-city"
  ask public-transports with [status = "traveling-to-city" and need-action?] [
    fd ((10 * update-interval) / (car-travel-time + public-transport-delay))

    ; if the public transport arrived at the central parking
    if ((xcor > 5) and (ycor = 2)) [
      foreach passenger-list [
        passenger -> ask passenger [
          set status "doing-stuff-in-city"
          ;set hidden? true
          set xcor 15
          set ycor -9
          set time_on_road_to_central_parking ((ticks - time_on_road_to_central_parking) * update-interval)
          set counter-doing-stuff-in-city ((random-gamma time-in-city-alpha time-in-city-lamda) * 3600)
          set time-spent-in-city ((ceiling (counter-doing-stuff-in-city / update-interval)) * update-interval)
          set money-spent (money-spent + public-transport-fee)
          set need-action? false
        ]
      ]
      die
    ]
    set need-action? false
  ]

  ; processing the public transportation vehicles with status "pickup-passengers-to-satellite"
  ask public-transports with [status = "pickup-passengers-to-satellite" and need-action?] [
    let list-passengers-waiting-pickup-to-satellite sort-on [(- time-spent-at-central-parking)] public-transport-passengers with [status = "waiting-pickup-to-satellite" and need-action?]

    foreach list-passengers-waiting-pickup-to-satellite [
      passenger ->
      if (passenger-seats-left > 0) [
        set passenger-seats-left (passenger-seats-left - 1)
        set passenger-list (lput passenger passenger-list)
        ask passenger [
          set status "traveling-to-satellite"
          ;set hidden? true
          set xcor 15
          set ycor -9
          set type_to_satellite_parking breed
          set time_on_road_to_satellite_parking ticks
          set time-spent-at-central-parking (time-spent-at-central-parking + 1)
          set need-action? false
        ]
        set list-passengers-waiting-pickup-to-satellite remove-item 0 list-passengers-waiting-pickup-to-satellite
      ]
    ]
    set status "traveling-to-satellite"
    fd ((10 * update-interval) / (car-travel-time + public-transport-delay))
    set need-action? false
  ]

  ; processing the public transportation vehicles with status "traveling-to-satellite"
  ask public-transports with [status = "traveling-to-satellite" and need-action?] [
    fd ((10 * update-interval) / (car-travel-time + public-transport-delay))

    ; if the public transport arrived at the satellite parking
    if ((xcor < -5) and (ycor = 4)) [
      foreach passenger-list [
        passenger -> ask passenger [
          set status "heading-home"
          ;set hidden? false
          set color blue
          set xcor -12
          set ycor 1
          ask car-dummy [die]
          set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite + 1)
          set time_on_road_to_satellite_parking ((ticks - time_on_road_to_satellite_parking) * update-interval)
          set money-spent (money-spent + public-transport-fee)
          set timestamp-end sim-datetime
          set time-end ticks
          write-statistics
          set breed turtles
        ]
      ]
      die
    ]
    set need-action? false
  ]
end

; function for processing the public transportation passengers
to process-public-transport-passengers
  ; processing all public transportation passengers with status "parking-car"
  ask public-transport-passengers with [status = "parking-car" and need-action?] [
    ifelse (counter-parking-car <= 0) [
      set status "waiting-pickup-to-city"
    ] [
      set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
      set counter-parking-car (counter-parking-car - update-interval)
    ]
    set need-action? false
  ]

  ; processing the public transportation passengers with status "waiting-pickup-to-city"
  ask public-transport-passengers with [status = "waiting-pickup-to-city" and need-action?] [
    ; check if another option is more cost effcient than beeing a public transportation passenger
    let new-class ""
    ifelse (ridesharing?) [
      let cost-list (sublist (calculate-cost-functions "public-transport-passenger") 0 4)
      set new-class (item (position (min cost-list) cost-list) ["rideshare-driver" "rideshare-passenger" "solo-driver" "public-transport-passenger"])
    ] [
      let cost-list (sublist (calculate-cost-functions "public-transport-passenger") 2 4)
      set new-class (item (position (min cost-list) cost-list) ["solo-driver" "public-transport-passenger"])
    ]
    ; check if to change the class or not
    ifelse (new-class != "public-transport-passenger") [
      set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite + 1)
      set-new-class new-class
    ] [
      set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
    ]
    set need-action? false
  ]

  ; processing the public transportation passengers with status "doing-stuff-in-city"
  ask public-transport-passengers with [status = "doing-stuff-in-city" and need-action?] [
    ; if the passenger is done with his duties in the city
    ifelse (counter-doing-stuff-in-city <= 0) [
      ; check if another option is more cost effcient than beeing a public transportation passenger
      let new-class ""
      ifelse ridesharing? [
        let cost-list (sublist (calculate-cost-functions "public-transport-passenger") 4 6)
        set new-class (item (position (min cost-list) cost-list) ["rideshare-passenger" "public-transport-passenger"])
      ] [
        set new-class "public-transport-passenger"
      ]
      set-new-class-way-back new-class
    ] [
      set counter-doing-stuff-in-city (counter-doing-stuff-in-city - update-interval)
    ]
    set need-action? false
  ]

  ; processing the public transportation passengers with status "waiting-pickup-to-satellite"
  ask public-transport-passengers with [status = "waiting-pickup-to-satellite" and need-action?] [
    ; check if another option is more cost effcient than beeing a public transportation passenger
    let new-class ""
    ifelse ridesharing? [
      let cost-list (sublist (calculate-cost-functions "public-transport-passenger") 4 6)
      set new-class (item (position (min cost-list) cost-list) ["rideshare-passenger" "public-transport-passenger"])
    ] [
      set new-class "public-transport-passenger"
    ]
    ; check if to change the class or not
    ifelse (new-class = "rideshare-passenger") [
      set-new-class-way-back new-class
    ] [
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
    ]
    set need-action? false
  ]
end
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




; helper function section
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
; function for writing a string to the output file (and the output area)
to write-output [string]
  file-print string
  if (display?) [
    output-print string
  ]
end

; function reporting a random number (integer) in the given interval (including extremes)
to-report random-in-range [min-extreme max-extreme]
  report (min-extreme + random (max-extreme - min-extreme + 1))
end

; function reporting the weighted mean given the old mean and a new value to consider
to-report calculate-weighted-mean [new-value current-mean]
  report ((0.5 * current-mean) + (0.5 * new-value))
end

; function calculating the mean and variance given the old mean and a new value to consider
to-report calculate-mean-and-variance [new-value current-mean M2 counter]
  set counter (counter + 1)
  let delta (new-value - current-mean)
  set current-mean (current-mean + (delta / counter))
  let delta2 (new-value - current-mean)
  set M2 (M2 + delta * delta2)

  ifelse (counter > 1) [
    let var (M2 / (counter - 1))
    report (list current-mean var M2 counter)
  ] [
    report (list current-mean 0 M2 counter)
  ]
end

; function updating/decreasing the mean values (adding a new value of 0) if they were not updated within the given interval (mean-update-if-no-change)
to update-means-if-no-change
  if (((ticks - (item 1 mean-waiting-time-pickup-to-city)) * update-interval) >= mean-update-if-no-change) [
    set mean-waiting-time-pickup-to-city (list (calculate-weighted-mean 0 (item 0 mean-waiting-time-pickup-to-city)) ticks)
  ]
  if (((ticks - (item 1 mean-waiting-time-pickup-to-satellite)) * update-interval) >= mean-update-if-no-change) [
    set mean-waiting-time-pickup-to-satellite (list (calculate-weighted-mean 0 (item 0 mean-waiting-time-pickup-to-satellite)) ticks)
  ]
  if (((ticks - (item 1 mean-solo-driver-waiting-for-parking-lot)) * update-interval) >= mean-update-if-no-change) [
    set mean-solo-driver-waiting-for-parking-lot (list (calculate-weighted-mean 0 (item 0 mean-solo-driver-waiting-for-parking-lot)) ticks)
  ]
  if (((ticks - (item 1 mean-rideshare-driver-waiting-for-parking-lot)) * update-interval) >= mean-update-if-no-change) [
    set mean-rideshare-driver-waiting-for-parking-lot (list (calculate-weighted-mean 0 (item 0 mean-rideshare-driver-waiting-for-parking-lot)) ticks)
  ]
end

; function updating the simulations datetime
to update-sim-datetime
  let total-seconds 0
  ifelse (new-simulation?) [
    set total-seconds (ticks * update-interval)
  ] [
    set total-seconds ((ticks + 1) * update-interval)
  ]

  set sim-day (1 + (floor(total-seconds / 86400)))
  let seconds-left (total-seconds - ((sim-day - 1) * 86400))
  set sim-hour floor(seconds-left / 3600)
  set seconds-left (seconds-left - (sim-hour * 3600))
  set sim-minute floor(seconds-left / 60)
  set sim-second (seconds-left - (sim-minute * 60))

  set sim-datetime get-sim-datetime
end

; function reporting the string representation (format: day D | HH:MM:SS) of the simulations datetime
to-report get-sim-datetime
  let str-sim-day sim-day

  let str-sim-hour ""
  ifelse (sim-hour < 10) [
    set str-sim-hour (word "0" sim-hour)
  ] [
    set str-sim-hour sim-hour
  ]

  let str-sim-minute ""
  ifelse (sim-minute < 10) [
    set str-sim-minute (word "0" sim-minute)
  ] [
    set str-sim-minute sim-minute
  ]

  let str-sim-second ""
  ifelse (sim-second < 10) [
    set str-sim-second (word "0" sim-second)
  ] [
    set str-sim-second sim-second
  ]

  report (word "day " str-sim-day " | " str-sim-hour ":" str-sim-minute ":" str-sim-second)
end

; function reporting the traffic-distribution for a given time (in seconds from 0:00) of the day
to-report traffic-distribution [time]
  let x (time mod 86400)
  report ((0.6 * (normal-distribution x (8 * 3600) (1 * 3600))) + (4 * (normal-distribution x (12 * 3600) (5 * 3600))) +  (3 * (normal-distribution x (17 * 3600) (3 * 3600))))
  ;TODO add other function to test the impact?!
end

; normal distribution function
to-report normal-distribution [x mu sigma]
  report ((1 / (sigma * (sqrt (2 * pi)))) * (exp (  (- (1 / 2)) * (((x - mu) / sigma)^ 2))))
end


; function reporting the costs for an agent to become any type of traveler at the "current" time step
to-report calculate-cost-functions [whoo]
  let V_t value-of-time ;the persons "value of time"

  let max-solo-driver-waiting-time 0 ;the time in seconds of the solo driver waiting the longest in the queue for getting a parking space
  let max-rideshare-driver-waiting-time 0 ;the time in seconds of the rideshare driver waiting the longest in the queue for getting a parking space
  let list-max-solo-driver-waiting-time [counter-waiting-free-parking-space] of (solo-drivers with [status = "waiting-free-parking-space"] with-max [counter-waiting-free-parking-space])
  let list-max-rideshare-driver-waiting-time [counter-waiting-free-parking-space] of (rideshare-drivers with [status = "waiting-free-parking-space"] with-max [counter-waiting-free-parking-space])
  if (length list-max-solo-driver-waiting-time) > 0 [
    set max-solo-driver-waiting-time ((item 0 list-max-solo-driver-waiting-time) * update-interval)
  ]
  if (length list-max-rideshare-driver-waiting-time) > 0 [
    set max-rideshare-driver-waiting-time ((item 0 list-max-rideshare-driver-waiting-time) * update-interval)
  ]

  let t_cs max (list max-solo-driver-waiting-time (item 0 mean-solo-driver-waiting-for-parking-lot)) ;the estimated average time searching for a parking lot in central parking for solo drivers
  let t_cr max (list max-rideshare-driver-waiting-time (item 0 mean-rideshare-driver-waiting-for-parking-lot)) ;the estimated average time searching for a parking lot in central parking for rideshare drivers
  let f_c regular-parking-fee ;the central parking fee for solo drivers
  let f_d driving-costs ;the costs for driving (e.g gas, car usage, congestion charge)

  let num-people-waiting-to-city 0 ;the number of people waiting for a public transportation vehicle to the central parking
  let num-people-waiting-to-satellite 0 ;the number of people waiting for a public transportation vehicle to the satellite parking
  ifelse (whoo = "public-transport-passenger") [
    set num-people-waiting-to-city (count public-transport-passengers with [status = "waiting-pickup-to-city"])
    set num-people-waiting-to-satellite (count public-transport-passengers with [status = "waiting-pickup-to-satellite"])
  ][
    set num-people-waiting-to-city ((count public-transport-passengers with [status = "waiting-pickup-to-city"]) + 1)
    set num-people-waiting-to-satellite ((count public-transport-passengers with [status = "waiting-pickup-to-satellite"]) + 1)
  ]

  let waiting-time-next-public-transport-to-city (((floor (num-people-waiting-to-city / max-num-passenger-seats-public-transport)) * public-transport-interval) + (public-transport-interval - ((ticks * update-interval) mod public-transport-interval))) ;the waiting time in seconds for the next free public transportation vehicle
  let waiting-time-next-public-transport-to-satellite (((floor (num-people-waiting-to-satellite / max-num-passenger-seats-public-transport)) * public-transport-interval) + (public-transport-interval - ((ticks * update-interval) mod public-transport-interval))) ;the waiting time in seconds for the next free public transportation vehicle
  let t_pt_c (public-transport-delay + waiting-time-next-public-transport-to-city) ;the estimated travel delay caused by public transit on the way to the central parking
  let t_pt_s (public-transport-delay + waiting-time-next-public-transport-to-satellite) ;the estimated travel delay caused by public transit on the way to the satellite parking

  let f_pt public-transport-fee ;the public transport fee for passengers
  let R_d (f_d) ;the costs for the return from the city center for drivers
  let R_p (f_pt + (((public-transport-interval / 2) + public-transport-delay) * V_t)) ;the costs for the return from the city center for passengers (assumed: worst case scenario public transport with a waiting time of the halfe public transport interval)
  let W willingness ;the persons willingness for ridesharing
  let f_r rideshare-parking-fee ;the central parking fee for ridesharing vehicles
  let f_rs ((f_r + f_d) / (max-num-passenger-seats-car)) ;the cost split for ridesharing   ;TODO +1 for optimistic estimation (car always full)

  let max-passenger-waiting-time-to-city 0 ;the time in seconds of the rideshare passenger waiting the longest for getting picked up the the central parking
  let max-passenger-waiting-time-to-satellite 0 ;the time in seconds of the rideshare passenger waiting the longest for getting picked up the the satellite parking
  let list-max-passenger-waiting-time-to-city [time-spent-at-satellite-parking] of (rideshare-passengers with [status = "waiting-pickup-to-city"] with-max [time-spent-at-satellite-parking])
  let list-max-passenger-waiting-time-to-satellite [time-spent-at-central-parking] of (rideshare-passengers with [status = "waiting-pickup-to-satellite"] with-max [time-spent-at-central-parking])
  if (length list-max-passenger-waiting-time-to-city) > 0 [
    set max-passenger-waiting-time-to-city ((item 0 list-max-passenger-waiting-time-to-city) * update-interval)
  ]
  if (length list-max-passenger-waiting-time-to-satellite) > 0 [
    set max-passenger-waiting-time-to-satellite ((item 0 list-max-passenger-waiting-time-to-satellite) * update-interval)
  ]
  let t_r max (list max-passenger-waiting-time-to-city (item 0 mean-waiting-time-pickup-to-city)) ;the estimated time waiting for a ride to the central parking
  let t_rr max (list max-passenger-waiting-time-to-satellite (item 0 mean-waiting-time-pickup-to-satellite)) ;the estimated time waiting for a ride to the satellite parking

  let U_D 0 ;the costs beeing a rideshare driver
  ifelse (whoo = "rideshare-passenger") [
    ifelse ( ((count (rideshare-passengers with [status = "waiting-pickup-to-city"])) - 1) > 0) [
      set U_D (W + (t_cr * V_t) + f_rs + (R_d / (max-num-passenger-seats-car))) ;TODO +1 for optimistic estimation (car always full)
    ] [
      set U_D 10000 ;TODO set a high value if there is no passenger waiting....
    ]
  ] [
    ifelse ( (count (rideshare-passengers with [status = "waiting-pickup-to-city"])) > 0) [
      set U_D (W + (t_cr * V_t) + f_rs + (R_d / (max-num-passenger-seats-car))) ;TODO +1 for optimistic estimation (car always full)
    ] [
      set U_D 10000 ;TODO set a high value if there is no passenger waiting....
    ]
  ]

  let U_P (W + (t_r * V_t) + f_rs + R_p) ;the costs beeing a rideshare passenger
  let U_S ((t_cs * V_t) + f_c + f_d + R_d) ;the costs beeing a solo driver
  let U_T (f_pt + (t_pt_c * V_t) + R_p) ;the costs beeing a public transport passenger
  let U_PB (W + (t_rr * V_t) + (R_d / (max-num-passenger-seats-car))) ;the costs beeing a rideshare passenger on the way back
  let U_TB (f_pt + (t_pt_s * V_t)) ;the costs beeing a public transport passenger on the way back

  report (list U_D U_P U_S U_T U_PB U_TB)
end

; function for setting/changing the type/breed of an agent
to set-new-class [new-class]
  ; removing the dummy car if a passenger becomes another class
  if ((is-rideshare-passenger? self) or (is-public-transport-passenger? self)) [ask car-dummy [die]]

  ifelse new-class = "rideshare-driver" [
    set breed rideshare-drivers
    set color red
    set xcor -8
    set ycor -3
    set status "pickup-passengers-to-city"
    set passenger-seats-left max-num-passenger-seats-car
    set passenger-list []
    set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
    set used-parking-space ""
    set need-action? false
    ] [ ifelse new-class = "rideshare-passenger" [
      set breed rideshare-passengers
      set color blue
      set xcor random-in-range -9 -8
      set ycor random-in-range -5 -4
      set status "waiting-pickup-to-city"
      set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)

      let dummy 0
      hatch-dummy-parking-cars 1 [
        set dummy self
        set status "parking"
        set owner myself
        set no-class? false
        set xcor random-in-range -11 -10
        set ycor random-in-range -5 -4
      ]
      set car-dummy dummy
      set used-parking-space "satellite"
      set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite - 1)
      set need-action? false
      ] [ ifelse new-class = "solo-driver" [
        set breed solo-drivers
        set color blue
        set xcor -5
        set ycor random-in-range -4 -3
        set status "traveling-to-city"
        set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)
        set used-parking-space ""
        set type_to_central_parking breed
        set time_on_road_to_central_parking ticks
        fd ((10 * update-interval) / car-travel-time)
        set need-action? false
        ] [ if new-class = "public-transport-passenger" [
          set breed public-transport-passengers
          set color blue
          set xcor -6
          set ycor 2
          set status "waiting-pickup-to-city"
          set time-spent-at-satellite-parking (time-spent-at-satellite-parking + 1)

          let dummy 0
          hatch-dummy-parking-cars 1 [
            set dummy self
            set status "parking"
            set owner myself
            set no-class? false
            set xcor random-in-range -11 -10
            set ycor random-in-range -5 -4
          ]
          set car-dummy dummy
          set used-parking-space "satellite"
          set num-free-parking-spaces-satellite (num-free-parking-spaces-satellite - 1)
          set need-action? false
  ]]]]
end

; function for setting/changing the type/breed of an agent
to set-new-class-way-back [new-class]
  ifelse new-class = "rideshare-passenger" [
    set breed rideshare-passengers
    set color blue
    set xcor random-in-range 8 9
    set ycor random-in-range -5 -4
    set heading 270
    set status "waiting-pickup-to-satellite"
    set time-spent-at-central-parking (time-spent-at-central-parking + 1)
    set need-action? false
    ] [if new-class = "public-transport-passenger" [
      set breed public-transport-passengers
      set color blue
      set xcor 6
      set ycor 4
      set heading 270
      set status "waiting-pickup-to-satellite"
      set time-spent-at-central-parking (time-spent-at-central-parking + 1)
      set need-action? false
  ]]
end

; function for creating the output statitics
to write-statistics
  if ((time-start >= sim-start-tick) and (time-start <= sim-end-tick)) [
    let id who
    let total-time ((time-end - time-start) * update-interval)
    let waiting_time_at_satellite_parking (time-spent-at-satellite-parking * update-interval)
    let waiting_time_at_central_parking (time-spent-at-central-parking * update-interval)
    let waiting-time-free-parking-space 0
    let time-parking-car-at-central-parking 0
    carefully [
      set waiting-time-free-parking-space (counter-waiting-free-parking-space * update-interval)
      set time-parking-car-at-central-parking parking-time
    ] [
      set waiting-time-free-parking-space 0
      set time-parking-car-at-central-parking 0
    ]

    let out (word
      "0" seperator ;since two different statistics are written to the same output file, this flag indicating which one, so lines can be filtered
      id seperator
      type_to_central_parking seperator
      type_to_satellite_parking seperator
      used-parking-space seperator
      total-time seperator
      time-start seperator
      time-end seperator
      timestamp-start seperator
      timestamp-end seperator
      waiting_time_at_satellite_parking seperator
      waiting_time_at_central_parking seperator
      waiting-time-free-parking-space seperator
      time-parking-car-at-central-parking seperator
      time_on_road_to_central_parking seperator
      time_on_road_to_satellite_parking seperator
      time-spent-in-city seperator
      value-of-time seperator
      willingness seperator
      money-spent
    )

    ;TODO: passenger +30?!? ERROR!!!   (waiting_time_at_satellite_parking + time_on_road_to_central_parking + waiting-time-free-parking-space + time-spent-in-city + waiting_time_at_central_parking + time_on_road_to_satellite_parking) seperator
    ;set out(word
    ;  type_to_central_parking seperator
    ;  type_to_satellite_parking seperator
    ;  waiting_time_at_satellite_parking seperator
    ;  waiting_time_at_central_parking seperator
    ;  "\t" seperator
    ;  "\t" seperator
    ;  total-time seperator
    ;  (waiting_time_at_satellite_parking + time_on_road_to_central_parking + waiting-time-free-parking-space + time-parking-car-at-central-parking + time-spent-in-city + waiting_time_at_central_parking + time_on_road_to_satellite_parking) seperator
    ;)

    write-output out
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
577
89
1198
474
-1
-1
19.8
1
10
1
1
1
0
1
1
1
-15
15
-9
9
0
0
1
ticks
30.0

BUTTON
579
49
700
82
setup simulation
setup-simulation 1 1 1 1
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
710
10
830
43
run simulation
run-simulation
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

INPUTBOX
4
73
280
133
update-interval
30.0
1
0
Number

SWITCH
579
10
699
43
display?
display?
0
1
-1000

MONITOR
984
480
1158
525
num-rs-waiting-pickup-to-city
count rideshare-passengers with [status = \"waiting-pickup-to-city\"]
17
1
11

MONITOR
577
480
756
525
NIL
count solo-drivers
17
1
11

MONITOR
577
529
756
574
NIL
count public-transport-passengers
17
1
11

MONITOR
578
579
756
624
NIL
count rideshare-drivers
17
1
11

MONITOR
578
630
756
675
NIL
count rideshare-passengers
17
1
11

BUTTON
711
49
831
82
stop simulation
stop-simulation
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

INPUTBOX
288
73
563
133
time-period-to-simulate
86400.0
1
0
Number

BUTTON
842
29
961
62
restore default inputs
restore-default-input-values
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
5
10
563
70
output-filename
output\\SIMULATION
1
0
String

MONITOR
986
580
1159
625
num-rs-waiting-pickup-to-satellite
count rideshare-passengers with [status = \"waiting-pickup-to-satellite\"]
17
1
11

INPUTBOX
2
446
189
506
num-parking-spaces-satellite
5000.0
1
0
Number

INPUTBOX
192
446
378
506
num-regular-parking-spaces-city
400.0
1
0
Number

INPUTBOX
381
446
566
506
num-ridesharing-parking-spaces-city
0.0
1
0
Number

MONITOR
762
480
976
525
NIL
num-free-parking-spaces-satellite
17
1
11

MONITOR
762
529
977
574
NIL
num-free-regular-parking-spaces-city
17
1
11

MONITOR
763
579
977
624
NIL
num-free-ridesharing-parking-spaces-city
17
1
11

BUTTON
970
49
1056
82
run once
run-simulation
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
2
318
279
378
public-transport-interval
1800.0
1
0
Number

INPUTBOX
288
255
563
315
public-transport-delay
900.0
1
0
Number

INPUTBOX
3
254
279
314
car-travel-time
1800.0
1
0
Number

INPUTBOX
2
382
189
442
regular-parking-fee
10.0
1
0
Number

INPUTBOX
193
382
378
442
rideshare-parking-fee
5.0
1
0
Number

INPUTBOX
1625
535
1737
595
satellite-parking-fee
0.0
1
0
Number

MONITOR
1067
24
1172
69
NIL
sim-datetime
17
1
11

INPUTBOX
381
382
565
442
driving-costs
4.0
1
0
Number

INPUTBOX
287
318
564
378
public-transport-fee
3.0
1
0
Number

INPUTBOX
8
584
125
644
value-of-time-mean
25.0
1
0
Number

INPUTBOX
137
584
291
644
value-of-time-standard-deviation
10.0
1
0
Number

INPUTBOX
8
652
125
712
time-in-city-alpha
3.0
1
0
Number

INPUTBOX
137
651
292
711
time-in-city-lamda
2.0
1
0
Number

INPUTBOX
9
519
126
579
willingness-mean
5.0
1
0
Number

INPUTBOX
136
519
291
579
willingness-standard-deviation
5.0
1
0
Number

MONITOR
1165
480
1403
525
rs-mean-waiting-time-pickup-to-city
mean-waiting-time-pickup-to-city
17
1
11

BUTTON
970
10
1057
43
run all
run-all-simulations\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
3
190
279
250
num-agents
4000.0
1
0
Number

SWITCH
289
203
562
236
ridesharing?
ridesharing?
1
1
-1000

INPUTBOX
365
730
554
790
time-parking-in-empty-parking-garage
60.0
1
0
Number

INPUTBOX
204
730
359
790
max-time-parking
300.0
1
0
Number

OUTPUT
1203
89
1718
475
13

MONITOR
986
530
1159
575
num-pt-waiting-pickup-to-city
count public-transport-passengers with [status = \"waiting-pickup-to-city\"]
17
1
11

MONITOR
986
630
1161
675
num-pt-waiting-pickup-to-satellite
count public-transport-passengers with [status = \"waiting-pickup-to-satellite\"]
17
1
11

MONITOR
1166
530
1404
575
rs-mean-waiting-time-pickup-to-satellite
mean-waiting-time-pickup-to-satellite
17
1
11

MONITOR
1168
580
1404
625
NIL
mean-solo-driver-waiting-for-parking-lot
17
1
11

MONITOR
1169
631
1433
676
NIL
mean-rideshare-driver-waiting-for-parking-lot
17
1
11

TEXTBOX
1622
516
1772
534
Unused for now: TODO?!
11
0.0
1

INPUTBOX
325
526
548
586
max-num-passenger-seats-car
3.0
1
0
Number

INPUTBOX
332
593
548
653
max-num-passenger-seats-public-transport
50.0
1
0
Number

INPUTBOX
11
790
166
850
mean-update-if-no-change
630.0
1
0
Number

CHOOSER
11
731
153
776
central-parking-mode
central-parking-mode
"split" "shared"
1

INPUTBOX
306
797
461
857
satellite-parking-time
60.0
1
0
Number

INPUTBOX
289
136
562
196
sim-time-buffer
21600.0
1
0
Number

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

bus
false
0
Polygon -7500403 true true 15 206 15 150 15 120 30 105 270 105 285 120 285 135 285 206 270 210 30 210
Rectangle -16777216 true false 36 126 231 159
Line -7500403 false 60 135 60 165
Line -7500403 false 60 120 60 165
Line -7500403 false 90 120 90 165
Line -7500403 false 120 120 120 165
Line -7500403 false 150 120 150 165
Line -7500403 false 180 120 180 165
Line -7500403 false 210 120 210 165
Line -7500403 false 240 135 240 165
Rectangle -16777216 true false 15 174 285 182
Circle -16777216 true false 48 187 42
Rectangle -16777216 true false 240 127 276 205
Circle -16777216 true false 195 187 42
Line -7500403 false 257 120 257 207

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
