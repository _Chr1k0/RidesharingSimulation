'''
Parse n stats
'''

# Imports
# ----------------------------------------------------------------------------------------------------------------------
import os
import sys
import Tkinter, tkFileDialog
# ----------------------------------------------------------------------------------------------------------------------


def getdirectory():
    root = Tkinter.Tk()
    root.withdraw()
    return tkFileDialog.askdirectory()  


def displayhelp():
    print("HEEELP - How to use?\n"
          "    0. Make sure the you put all files you want to parse into one directory (and just them - no other files are allowed!)\n"
          "    1. Choose that directory\n"
          "    2. let the parser do his job\n"
          "    3. Done! :)")

# Dealing with files starting with 0


def dealEachFile_Zero(fin, fout, header):
    print "Deal with Zeros:" + fin    
      
    # input file        
    f = open(fin,'r')    
    
    # dictionaris to calc the number of users for each role to central or satellite parkings
    # type of user to central:number of the type of user
    r_central = {'solo-drivers':0, 'rideshare-drivers':0, 'rideshare-passengers':0,'public-transport-passengers':0}
    # type of user to satellite
    r_satellite = {'solo-drivers':0, 'rideshare-drivers':0, 'rideshare-passengers':0,'public-transport-passengers':0}
    # used parking space
    parkspace = {'central-regular':0,'satellite':0}
    #travel duration (tot time - time in city) including parking time n waiting to enter parking lot
    # types of users should consider the comb of inbound and outbound
    # sd - solo driver, rd - ridesharing driver, rp - ridesharing passenger, tp - public transit passenger
    t_dur = {'sdsd':0, 'sdrd':0,'rdsd':0,'rdrd':0,'rprp':0,'rptp':0,'tprp':0,'tptp':0, 'all':0}
    # counter for each combination of roles inbound and outbound
    c_dur = {'sdsd':0, 'sdrd':0,'rdsd':0,'rdrd':0,'rprp':0,'rptp':0,'tprp':0,'tptp':0, 'all':0}
    # waiting time at satellite parking grouped by type of user - only passengers
    wt_satellite = {'rideshare-passengers':0,'public-transport-passengers':0}
    # waiting time at central parking grouped by type of user - only passengers
    wt_central = {'rideshare-passengers':0,'public-transport-passengers':0}
    # waiting to enter the parking lot - only for drivers
    wt_free = {'solo-drivers':0, 'rideshare-drivers':0}
    # monetary cost of the travel - considering the combined roles of inbound n outbound
    # totalcost = money-spent + value-of-time * t_dur

    totalcost = {'sdsd':0, 'sdrd':0,'rdsd':0,'rdrd':0,'rprp':0,'rptp':0,'tprp':0,'tptp':0, 'all':0}
    money = {'sdsd':0, 'sdrd':0,'rdsd':0,'rdrd':0,'rprp':0,'rptp':0,'tprp':0,'tptp':0, 'all':0}
    # Input the header line
    if header:
        parameterLine = f.readline()[13:-2].replace(" ", ";")+';'
        #parameterLine = parameterLine.replace('\n', ';')
        fout.write(parameterLine)    # parameter
        for k in sorted(r_central):
            fout.write("r_central:"+k +";")
        for k in sorted(r_satellite):
            fout.write("r_satellite:"+k +";")
        for k in sorted(parkspace):
            fout.write("parkspace:"+k +";")
        for k in sorted(t_dur):
            fout.write("t_dur:"+k +";")
        for k in sorted(wt_satellite): # waiting to be picked up at satellite parking on the way to city centre
            fout.write("wt_satellite:"+k +";")
        for k in sorted(wt_central): # waiting to be picked up at central parking before going back to satellite parking
            fout.write("wt_central:"+k +";")
        for k in sorted(wt_free):  # DRIVERS waiting for free parking on the way to centre
            fout.write("wt_free:"+k +";")
        for k in sorted(totalcost):
            fout.write("totalcost:"+k +";")
        for k in sorted(money):
            fout.write("money:"+k +";")
        fout.write("\n")
    else:
        f.readline() #skip first line

    parameterLine = f.readline()[13:-2].replace(" ", ";")+';'
    #parameterLine = parameterLine.replace('\n', ';')
    # paramStr = f.readline()[13:-2].replace(" ", ";")
    #     paramStr2 = f.readline()[13:-2].replace(" ", ";")

    fout.write(parameterLine)    # parameter

    f.readline()    # skip the title line
    l = f.readline()
    while len(l) > 0:
        # contents of each line after being parsed
        c = l.strip('\n').split(';')
        
        if c[2] in r_central:
            r_central[c[2]] += 1
        else:
            r_central[c[2]] = 1
            
        if c[3] in r_satellite:
            r_satellite[c[3]] += 1
        else:
            r_satellite[c[3]] = 1

        if c[4] in parkspace:
            parkspace[c[4]] += 1
        else:
            parkspace[c[4]] = 1

        # travel duration
        t_dur['all'] += (long(c[5]) - long(c[16]))
        c_dur['all'] += 1
        totalcost['all'] += (float(c[19]) + (float(c[5]) - float(c[16])) * float(c[17]))  # money-spent + time_dur * value-of-time
        money['all'] += float(c[19])   # money-spent + time_dur * value-of-time

        if c[2] == 'solo-drivers': # role to city
            if c[3] == 'solo-drivers':  # role from city
                t_dur['sdsd'] += (long(c[5]) - long(c[16]))
                c_dur['sdsd'] += 1
                totalcost['sdsd'] += (float(c[19]) + (float(c[5]) - float(c[16])) * float(c[17]))  # money-spent + time_dur * value-of-time
                money['sdsd'] += float(c[19])
            elif c[3] == 'rideshare-drivers':
                t_dur['sdrd'] += (long(c[5]) - long(c[16]))
                c_dur['sdrd'] += 1
                totalcost['sdrd'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['sdrd'] += float(c[19])
            else:
                print c[2],c[3]
        if c[2] == 'rideshare-drivers':
            if c[3] == 'solo-drivers':
                t_dur['rdsd'] += (long(c[5]) - long(c[16]))
                c_dur['rdsd'] += 1
                totalcost['rdsd'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['rdsd'] += float(c[19])
            elif c[3] == 'rideshare-drivers':
                t_dur['rdrd'] += (long(c[5]) - long(c[16]))
                c_dur['rdrd'] += 1
                totalcost['rdrd'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['rdrd'] += float(c[19])
            else:
                print c[2],c[3]
        if c[2] == 'rideshare-passengers':
            if c[3] == 'rideshare-passengers':
                t_dur['rprp'] += (long(c[5]) - long(c[16]))
                c_dur['rprp'] += 1
                totalcost['rprp'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['rprp'] += float(c[19])
            elif c[3] == 'public-transport-passengers':
                t_dur['rptp'] += (long(c[5]) - long(c[16]))
                c_dur['rptp'] += 1
                totalcost['rptp'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['rptp'] += float(c[19])
            else:
                print c[2],c[3]
        if c[2] == 'public-transport-passengers':
            if c[3] == 'rideshare-passengers':
                t_dur['tprp'] += (long(c[5]) - long(c[16]))
                c_dur['tprp'] += 1
                totalcost['tprp'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))  # considering willingness
                money['tprp'] += float(c[19])
            elif c[3] == 'public-transport-passengers':
                t_dur['tptp'] += (long(c[5]) - long(c[16]))
                c_dur['tptp'] += 1
                totalcost['tptp'] += (float(c[18]) + float(c[19]) +  (float(c[5]) - float(c[16])) * float(c[17]))
                money['tptp'] += float(c[19])
            else:
                print c[2],c[3]

        # waiting-time-at-satellite should be corresponding to the role to central - passengers
        if c[2] in wt_satellite:
            wt_satellite[c[2]] += long(c[10])

        if c[3] in wt_central:
            wt_central[c[3]] += long(c[11])

        # waiting time for free parking - drivers
        if c[2] in wt_free:
            wt_free[c[2]] += long(c[12])
        
        l = f.readline()

    #output
    for k in sorted(r_central):
        fout.write(str(r_central[k])+";")
    for k in sorted(r_satellite):
        fout.write(str(r_satellite[k])+";")
    for k in sorted(parkspace):
        fout.write(str(parkspace[k])+";")
    for k in sorted(t_dur):
        if c_dur[k] > 0:
            v = float(t_dur[k]) / float(c_dur[k])   # average duration of this type
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")
    for k in sorted(wt_satellite): # waiting to be picked up at satellite parking on the way to city centre
        if r_central[k] > 0:
            v = float(wt_satellite[k]) / float(r_central[k])
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")
    for k in sorted(wt_central): # waiting to be picked up at central parking before going back to satellite parking
        if r_satellite[k] > 0:
            v = float(wt_central[k]) / float(r_satellite[k])
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")
    for k in sorted(wt_free):  # DRIVERS waiting for free parking on the way to centre
        if r_central[k] > 0:
            v = float(wt_free[k]) / float(r_central[k])
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")
    for k in sorted(totalcost):
        if c_dur[k] > 0:
            v = float(totalcost[k]) / float(c_dur[k])   # average duration of this type
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")
    for k in sorted(money):
        if c_dur[k] > 0:
            v = float(money[k]) / float(c_dur[k])   # average duration of this type
            fout.write(str(v)+";")
        else:
            fout.write(str(-1)+";")

    fout.write("\n")

      
def dealEachFile_One(fin, fout, header):   
    print "Deal with Ones:" + fin
    #TODO: ?!


def main():
    displayhelp()
    
    dir = getdirectory()
    #dir = "Test\\split\\"
    if not dir:   
        print("\nExiting!")
        sys.exit()
    else:        
        print("\nChosen directory: " + dir)

        #filename of the output
        fo0 = open(os.path.join(dir, "output", "0_stats.txt"), "w")
        fo1 = open(os.path.join(dir, "output", "1_stats.txt"), "w")
        
        writeheader_fo0 = True
        writeheader_fo1 = True
        
        # Loop each file (sorted by name) in the folder        
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        files.sort()
        
        print("Number of files found: " + str(len(files)) + "\n")       
        
        # Loop each file (sorted by name) in the folder        
        for f in files:
            print "Current file:" + f

            if f.startswith("0"): #f.split('-')[0] == "0":
                dealEachFile_Zero(os.path.join(dir,f), fo0, writeheader_fo0)
                writeheader_fo0 = False
            elif f.startswith("1"):
                dealEachFile_One(os.path.join(dir,f), fo1, writeheader_fo1)
                writeheader_fo1 = False           
            else:
                print ("Error: Can't handle file - " + f)
                
        fo0.close()    
        fo1.close()        
    print("\nExiting!")   
       
        
if __name__ == "__main__":
    main()